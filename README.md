# Introduction
A program written foremost for fun. It can sort the pixel of a given image in different ways, possibly with artistic results. ;)

## Supported sorting methods:
- Everything: Sorts the entire picture, from the first to the last pixel
- Vertical: Sorts each vertical line
- Horizontal: Sorts each horizontal line
- Difference: Sorts only in certain Intervals of the picture, details described below
- Difference with line-wrap: Same as the above, but wraps the intervals over multiple lines if necessary

## Supported comparison methods:
- Luminance (weighted): Compares each pixels luminance by weighing 0.2126 * red + 0.7152 * blue +
                0.0722 * green
- Luminance: Calculates the luminance by simply adding the RGB-values
- HSV: Compares the HSV-values with hue first, saturation second and value at last
- HSV (Step): Tries to eliminate noise by stretching the float-values of the former method to integers
- Red: Sorts the pixel by its relative red value
- Green: Sorts the pixel by its relative green value
- Blue: Sorts the pixel by its relative blue value

Interesting read on color-sorting:
[Alan Zucconi - The incredibly challenging task of sorting colours](https://www.alanzucconi.com/2015/09/30/colour-sorting/ "alanzucconi - The incredibly challenging task of sorting colours")

# Usage

Open an image by choosing File -> Open..., select a sorting method and a comparison method and click the "Start Sorting"-Button.

## Masking

A masking image allows for sorting only in certain regions of a given image. 
Open a masking image by choosing File -> Open masking...
The image should (ideally) be monochrome, but its size does not matter:
it will be stretched to the dimensions of the other image.

Check the "Use masking" checkbox and start sorting.
The PictureSorter will then only sort the intended image in the corresponding white regions of the masking image.

## Difference Settings

The first number gives the luminance (from 0, black to 255, white) for the first pixel from which to start a sort-interval, the second number gives the luminance difference between the first pixel and the last pixel, where the interval will end.

# Controls
- Mousewheel to zoom
- Middleclick to fit the picture to the viewport, same as clicking on the zoom-level-button

