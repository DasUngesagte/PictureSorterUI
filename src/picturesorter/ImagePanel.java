package picturesorter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;

/**
 * Repaints a BufferedImage
 *
 * @author DasUngesagte
 */
public class ImagePanel extends JPanel implements ActionListener {

    private BufferedImage image;
    private javax.swing.Timer timer;

    // Zooming:
    private boolean zoomer = false;
    private boolean reset = false;
    private double zoomFactor = 1d;
    private double zoomFactorZero = 1d;
    private AffineTransform at = new AffineTransform();

    /**
     * Constructor
     *
     * @param layout       LayoutManager for this JPanel
     * @param milliseconds Delay between timer-firings
     */
    ImagePanel(LayoutManager layout, int milliseconds, MouseAdapter mouseAdapter) {
        super(layout);
        timer = new Timer(milliseconds, this);
        this.addMouseListener(mouseAdapter);
        this.addMouseWheelListener(mouseAdapter);
        this.addMouseMotionListener(mouseAdapter);
    }

    void setImage(BufferedImage image) {
        this.image = image;
        this.repaint();
    }

    // Starts the timer
    void startAnimation() {
        timer.start();
    }

    // Stops the timer
    void stopAnimation() {

        timer.stop();
        repaint(); // One last repaint
    }

    @Override
    public Dimension getPreferredSize() {

        int width = Math.toIntExact(Math.round(image != null ? image.getWidth() * zoomFactor : 640));
        int height = Math.toIntExact(Math.round(image != null ? image.getHeight() * zoomFactor : 480));

        return new Dimension(width, height);
    }

    @Override
    protected void paintComponent(Graphics g) {

        super.paintComponent(g);

        // Zooming:
        Graphics2D g2 = (Graphics2D) g;
        if (zoomer) {
            at = new AffineTransform();
            at.scale(zoomFactor, zoomFactor);
            zoomer = false;
        }
        g2.transform(at);

        if (image != null) {

            g2.drawImage(image, 0, 0, image.getWidth(), image.getHeight(), this);
            g2.dispose();

            this.getParent().revalidate();
        }

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        repaint();
    }

    void setZoomFactor(double value) {
        zoomFactor = value;
        zoomer = true;
        repaint();
    }

    void resetZoomFactor() {

        if (image == null) return;

        zoomFactor = Math.min(this.getParent().getWidth() / (double) image.getWidth(),
                this.getParent().getHeight() / (double) image.getHeight());
        zoomer = true;
        repaint();
    }

    double getZoom() {
        return zoomFactor;
    }
}
