/*
 * Copyright (C) 2017 DasUngesagte
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package picturesorter;

import com.bulenkov.darcula.DarculaLaf;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.filechooser.FileFilter;
import javax.swing.plaf.basic.BasicLookAndFeel;
import javax.swing.text.NumberFormatter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DecimalFormat;

import static picturesorter.PictureSorter.ComparisonMode.*;
import static picturesorter.PictureSorter.SortingMode.*;

/**
 * Implements the GUI
 *
 * @author DasUngesagte
 */
class GUI {

    private static final GUI INSTANCE = new GUI();
    private PictureSorter controller;

    // Options
    private String inputpath = "";
    private String maskingpath = "";
    private boolean useMasking = false;
    private boolean invertSort = false;
    private boolean invertRandom = false;
    private boolean wrapLines = false;
    private PictureSorter.SortingMode sortMode = DIFFERENCE;
    private PictureSorter.ComparisonMode compMode = LUMINANCE_WEIGHTED;
    private int rotation = 0;

    // GUI-Elements
    private JFrame frame;
    private JFileChooser chooser;
    private JLabel statusLabel;
    private ImagePanel imagePanel;
    private JPanel diffPanel;
    private JPanel randPanel;
    private JScrollPane settingsPanel;
    private JSlider rotSlider;
    private JSpinner lumField;
    private JSpinner difField;
    private JSpinner startField;
    private JSpinner endField;
    private JSlider zoomSlider;
    private JButton zoomButton;
    private JButton sortButton;
    private boolean finishedSorting = true;


    private GUI() {
    }

    /******************************************************************************************************************
     *                                                 GETTER                                                         *
     ******************************************************************************************************************/

    // Singleton:
    static GUI getInstance() {
        return INSTANCE;
    }

    /******************************************************************************************************************
     *                                                 METHODS                                                        *
     ******************************************************************************************************************/

    void buildGUI() {

        // Force AA on:
        System.setProperty("awt.useSystemAAFontSettings", "on");
        System.setProperty("swing.aatext", "true");

        UIManager.getFont("Label.font");

        BasicLookAndFeel darcula = new DarculaLaf();
        try {
            UIManager.setLookAndFeel(darcula);
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }

        // Build the frame:
        frame = new JFrame("PictureSorter");
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.setMinimumSize(new Dimension(640, 600));
        WindowAdapter listener = new WindowAdapter();
        frame.addWindowListener(listener);

        JPanel contentPane = new JPanel(new BorderLayout());

        MouseAdapter mouseAdapter = new MouseAdapter(this);

        imagePanel = new ImagePanel(new BorderLayout(), 16, mouseAdapter);
        JScrollPane scrollPane = new JScrollPane(imagePanel);
        scrollPane.getVerticalScrollBar().setUnitIncrement(16);
        scrollPane.getHorizontalScrollBar().setUnitIncrement(16);
        mouseAdapter.setImagePanel(imagePanel, scrollPane);

        contentPane.add(scrollPane, BorderLayout.CENTER);

        // File chooser with filters:
        chooser = new JFileChooser();

        FileFilter jpgFilter = new ImageFileFilter("JPG and JPEG", new String[]{"JPG", "JPEG"});
        FileFilter pngFilter = new ImageFileFilter("PNG", "PNG");
        FileFilter bmpFilter = new ImageFileFilter("BMP", "BMP");
        FileFilter gifFilter = new ImageFileFilter("GIF", "GIF");
        chooser.addChoosableFileFilter(jpgFilter);
        chooser.addChoosableFileFilter(pngFilter);
        chooser.addChoosableFileFilter(bmpFilter);
        chooser.addChoosableFileFilter(gifFilter);


        // Add rest:
        frame.setJMenuBar(buildMenu());
        frame.setContentPane(contentPane);
        settingsPanel = buildSettingsPanel();
        frame.add(settingsPanel, BorderLayout.EAST);
        frame.add(buildStatusBar(), BorderLayout.SOUTH);

        frame.pack();
        frame.setVisible(true);

    }

    private JScrollPane buildSettingsPanel() {

        JPanel settingsPanel = new JPanel(new GridBagLayout());
        JScrollPane scrollPane = new JScrollPane(settingsPanel);
        scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPane.getVerticalScrollBar().setUnitIncrement(16);

        scrollPane.setBorder(BorderFactory.createEmptyBorder());
        settingsPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 15));
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.anchor = GridBagConstraints.PAGE_START;

        JLabel sortingMethodsLabel = new JLabel("Sorting ranges:");

        ButtonGroup sortingMethodButtons = new ButtonGroup();
        JRadioButtonMenuItem radioEvery = new JRadioButtonMenuItem("All");
        JRadioButtonMenuItem radioDiffe = new JRadioButtonMenuItem("Difference");
        JRadioButtonMenuItem radioRandom = new JRadioButtonMenuItem("Random");
        JRadioButtonMenuItem radioRandomG = new JRadioButtonMenuItem("Random with gaps");
        radioDiffe.setSelected(true);
        sortingMethodButtons.add(radioEvery);
        sortingMethodButtons.add(radioDiffe);
        sortingMethodButtons.add(radioRandom);
        sortingMethodButtons.add(radioRandomG);
        radioEvery.addActionListener((ActionEvent e) -> {
            diffPanel.setVisible(false);
            randPanel.setVisible(false);
            sortMode = LINES;
        });
        radioDiffe.addActionListener((ActionEvent e) -> {
            diffPanel.setVisible(true);
            randPanel.setVisible(false);
            sortMode = DIFFERENCE;
        });
        radioRandom.addActionListener((ActionEvent e) -> {
            diffPanel.setVisible(false);
            randPanel.setVisible(true);
            sortMode = RANDOM;
        });
        radioRandomG.addActionListener((ActionEvent e) -> {
            diffPanel.setVisible(false);
            randPanel.setVisible(true);
            sortMode = RANDOM_GAPS;
        });

        int grid_counter = 0;

        c.gridx = 0;
        c.gridy = grid_counter++;
        c.insets = new Insets(4, 2, 4, 2);
        settingsPanel.add(sortingMethodsLabel, c);
        c.gridy = grid_counter++;
        c.insets = new Insets(0, 0, 0, 0);
        settingsPanel.add(radioEvery, c);
        c.gridy = grid_counter++;
        settingsPanel.add(radioDiffe, c);
        c.gridy = grid_counter++;
        settingsPanel.add(radioRandom, c);
        c.gridy = grid_counter++;
        settingsPanel.add(radioRandomG, c);
        c.gridy = grid_counter++;
        c.insets = new Insets(4, 2, 4, 2);
        settingsPanel.add(new JSeparator(JSeparator.HORIZONTAL), c);

        JLabel comparisonMethodsLabel = new JLabel("Sort by:");

        ButtonGroup comparisonMethodButtons = new ButtonGroup();
        JRadioButtonMenuItem radioLumW = new JRadioButtonMenuItem("Luminance (weighted)");
        JRadioButtonMenuItem radioLum = new JRadioButtonMenuItem("Luminance");
        JRadioButtonMenuItem radioRed = new JRadioButtonMenuItem("Red");
        JRadioButtonMenuItem radioGreen = new JRadioButtonMenuItem("Green");
        JRadioButtonMenuItem radioBlue = new JRadioButtonMenuItem("Blue");
        JRadioButtonMenuItem radioHSV = new JRadioButtonMenuItem("HSV");
        JRadioButtonMenuItem radioHSVStep = new JRadioButtonMenuItem("HSV (step)");
        radioLumW.setSelected(true);
        comparisonMethodButtons.add(radioLumW);
        comparisonMethodButtons.add(radioLum);
        comparisonMethodButtons.add(radioRed);
        comparisonMethodButtons.add(radioGreen);
        comparisonMethodButtons.add(radioBlue);
        comparisonMethodButtons.add(radioHSV);
        comparisonMethodButtons.add(radioHSVStep);
        radioLumW.addActionListener((ActionEvent e) -> compMode = LUMINANCE_WEIGHTED);
        radioLum.addActionListener((ActionEvent e) -> compMode = LUMINANCE);
        radioRed.addActionListener((ActionEvent e) -> compMode = RED);
        radioGreen.addActionListener((ActionEvent e) -> compMode = GREEN);
        radioBlue.addActionListener((ActionEvent e) -> compMode = BLUE);
        radioHSV.addActionListener((ActionEvent e) -> compMode = HSV);
        radioHSVStep.addActionListener((ActionEvent e) -> compMode = HSV_STEP);

        c.gridy = grid_counter++;
        settingsPanel.add(comparisonMethodsLabel, c);
        c.gridy = grid_counter++;
        c.insets = new Insets(0, 0, 0, 0);
        settingsPanel.add(radioLumW, c);
        c.gridy = grid_counter++;
        settingsPanel.add(radioLum, c);
        c.gridy = grid_counter++;
        settingsPanel.add(radioHSV, c);
        c.gridy = grid_counter++;
        settingsPanel.add(radioHSVStep, c);
        c.gridy = grid_counter++;
        settingsPanel.add(radioRed, c);
        c.gridy = grid_counter++;
        settingsPanel.add(radioGreen, c);
        c.gridy = grid_counter++;
        settingsPanel.add(radioBlue, c);
        c.gridy = grid_counter++;
        c.insets = new Insets(4, 2, 4, 2);
        settingsPanel.add(new JSeparator(JSeparator.HORIZONTAL), c);

        JCheckBoxMenuItem wrapLinesBox = new JCheckBoxMenuItem("Wrap over lines");
        wrapLinesBox.addItemListener((ItemEvent e) -> wrapLines = wrapLinesBox.isSelected());
        c.gridy = grid_counter++;
        c.insets = new Insets(0, 0, 0, 0);
        settingsPanel.add(wrapLinesBox, c);

        JCheckBoxMenuItem invertSortBox = new JCheckBoxMenuItem("Invert sorting");
        invertSortBox.addItemListener((ItemEvent e) -> invertSort = invertSortBox.isSelected());
        c.gridy = grid_counter++;
        c.insets = new Insets(0, 0, 0, 0);
        settingsPanel.add(invertSortBox, c);

        JCheckBoxMenuItem masking = new JCheckBoxMenuItem("Use masking");
        masking.addItemListener((ItemEvent e) -> useMasking = masking.isSelected());
        c.gridy = grid_counter++;
        settingsPanel.add(masking, c);

        c.gridy = grid_counter++;
        c.insets = new Insets(4, 2, 10, 2);
        settingsPanel.add(new JSeparator(JSeparator.HORIZONTAL), c);

        c.gridy = grid_counter++;
        settingsPanel.add(new JLabel("Rotation:"), c);

        c.gridy = grid_counter++;
        rotSlider = new JSlider(JSlider.HORIZONTAL, 0, 270, 90);
        rotSlider.setValue(0);
        rotSlider.setMajorTickSpacing(90);
        rotSlider.setMinorTickSpacing(90);
        rotSlider.setPaintTicks(true);
        rotSlider.setPaintLabels(true);
        rotSlider.setSnapToTicks(true);
        rotSlider.setFocusable(false);
        rotSlider.addChangeListener((ChangeEvent e) -> {
            if(!rotSlider.getValueIsAdjusting()) {
                rotation = rotSlider.getValue();
                controller.rotateImage(rotation);
            }
        });
        settingsPanel.add(rotSlider, c);

        c.gridy = grid_counter++;
        c.insets = new Insets(4, 2, 10, 2);
        settingsPanel.add(new JSeparator(JSeparator.HORIZONTAL), c);

        c.gridy = grid_counter++;
        c.weighty = 0.0;
        c.insets = new Insets(0, 0, 0, 0);
        diffPanel = buildDifferenceSettings();
        randPanel = buildRandomSettings();
        settingsPanel.add(diffPanel, c);
        settingsPanel.add(randPanel, c);


        c.anchor = GridBagConstraints.PAGE_END;
        c.gridy = grid_counter;
        c.weighty = 0.5;
        settingsPanel.add(buildButtonPanel(), c);

        return scrollPane;
    }

    private JPanel buildButtonPanel() {

        JPanel buttonPanel = new JPanel(new GridBagLayout());
        GridBagConstraints cb = new GridBagConstraints();
        cb.fill = GridBagConstraints.HORIZONTAL;

        JButton resetButton = new JButton("Reset");
        resetButton.addActionListener((ActionEvent e) -> {

            if (inputpath.equals("")) {
                statusLabel.setText("No input specified!");
                return;
            } else if (!finishedSorting) {
                statusLabel.setText("Still sorting!");
                return;
            }

            // Reset image:
            controller.resetImage();
            imagePanel.setImage(controller.getImage());

            // Reset settings:
            rotSlider.setValue(0);

            statusLabel.setText("Reset!");

        });
        cb.weightx = 0.0;
        cb.gridy = 0;
        cb.gridx = 0;
        buttonPanel.add(resetButton, cb);

        sortButton = new JButton("Start sorting");
        sortButton.addActionListener((ActionEvent e) -> {

            if (inputpath.equals("")) {
                statusLabel.setText("No input specified!");
                return;
            } else if (maskingpath.equals("") && useMasking) {
                statusLabel.setText("No masking image specified!");
                return;
            } else if (!finishedSorting) {
                // Cancel-functionality:
                controller.cancelSort();
                return;
            }

            // Reset Image:
            //controller.resetImage();
            //imagePanel.setImage(controller.getImage());

            finishedSorting = false;
            statusLabel.setText("Sorting...");
            sortButton.setText("Cancel");
            imagePanel.startAnimation();
            controller.initSort();

        });
        cb.weightx = 1.0;
        cb.gridy = 0;
        cb.gridx = 1;
        buttonPanel.add(sortButton, cb);

        return buttonPanel;

    }

    private JPanel buildStatusBar() {

        JPanel statusBar = new JPanel(new BorderLayout());
        statusBar.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 0));

        statusLabel = new JLabel("Ready");
        statusBar.add(statusLabel, BorderLayout.LINE_START);

        //****************************** ZOOM-STUFF ***************************************

        JPanel zoomPanel = new JPanel(new FlowLayout(FlowLayout.TRAILING));

        zoomButton = new JButton("1.0x");
        zoomPanel.add(zoomButton);

        zoomSlider = new JSlider(JSlider.HORIZONTAL,
                10, 500, 100);

        zoomSlider.addChangeListener((ChangeEvent e) -> {

            double value = zoomSlider.getValue() / 100d;

            zoomButton.setText(new DecimalFormat("##.#").format(Double.valueOf(value)) + "x");
            imagePanel.setZoomFactor(value);
        });

        zoomButton.addActionListener((ActionEvent e) -> {
            imagePanel.resetZoomFactor();
            double factor = imagePanel.getZoom();
            zoomButton.setText(new DecimalFormat("##.#").format(Double.valueOf(factor)) + "x");
            zoomSlider.setValue(Math.toIntExact(Math.round(factor * 100)));
        });

        zoomPanel.add(zoomSlider);
        statusBar.add(zoomPanel, BorderLayout.LINE_END);
        return statusBar;
    }

    private JMenuBar buildMenu() {

        JMenuBar menuBar = new JMenuBar();
        menuBar.setBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3));

        //******************************** FILE MENU ******************************************
        JMenu fileMenu = new JMenu("File");

        JMenuItem openImage = new JMenuItem("Open Image...");
        openImage.addActionListener((ActionEvent e) -> {

            // We're still sorting:
            if (!finishedSorting) {
                statusLabel.setText("Sort ongoing!");
                return;
            }

            // Open file chooser, save input path, set PicSorter Image
            int selection = chooser.showOpenDialog(null);

            if (selection == JFileChooser.APPROVE_OPTION) {
                inputpath = chooser.getSelectedFile().getAbsolutePath();
                try {
                    controller.setImage(inputpath);
                    imagePanel.setImage(controller.getImage());
                    imagePanel.resetZoomFactor();
                    rotSlider.setValue(0);
                    zoomButton.setText(new DecimalFormat("##.#").format(Double.valueOf(imagePanel.getZoom())) + "x");
                    zoomSlider.setValue(Math.toIntExact(Math.round(imagePanel.getZoom() * 100)));
                } catch (IOException ex) {
                    showExceptionError(ex);
                    statusLabel.setText("Error while opening!");
                }

            }

            // Set status text:
            statusLabel.setText("Image successfully opened!");
        });

        JMenuItem saveImage = new JMenuItem("Save Image...");
        saveImage.addActionListener((ActionEvent e) -> {

            if (inputpath.equals("") || !finishedSorting) {
                statusLabel.setText("There is nothing to save!");
                return;
            }

            //Open file chooser, save input path, save PicSorter Image
            int selection = chooser.showSaveDialog(null);

            if (selection == JFileChooser.APPROVE_OPTION) {
                String outputpath = chooser.getSelectedFile().getAbsolutePath();
                String extension = chooser.getFileFilter().getDescription();
                String ext = outputpath.lastIndexOf(".") < 0 ? "" :
                        outputpath.substring(outputpath.lastIndexOf(".") + 1);
                String newExt;

                System.out.println("Extension: " + ext);

                if (ext.equals("jpg") || extension.equals("JPG and JPEG")) {
                    newExt = "jpg";
                    outputpath = !ext.equals("jpg") ? outputpath + "." + newExt : outputpath;
                } else if (ext.equals("gif") || extension.equals("GIF")) {
                    newExt = "gif";
                    outputpath = !ext.equals("gif") ? outputpath + "." + newExt : outputpath;
                } else if (ext.equals("bmp") || extension.equals("BMP")) {
                    newExt = "bmp";
                    outputpath = !ext.equals("bmp") ? outputpath + "." + newExt : outputpath;
                } else { // Default
                    newExt = "png";
                    outputpath = !ext.equals("png") ? outputpath + "." + newExt : outputpath;
                }

                try {
                    controller.saveImage(newExt, outputpath);
                } catch (IOException ex) {
                    showExceptionError(ex);
                    statusLabel.setText("Error while saving!");
                }
            }
        });

        JMenuItem openFilterImage = new JMenuItem("Open Masking Image...");
        openFilterImage.addActionListener((ActionEvent e) -> {

            // Open file chooser, save input path, save PicSorter Image
            int selection = chooser.showOpenDialog(null);
            if (selection == JFileChooser.APPROVE_OPTION) {

                maskingpath = chooser.getSelectedFile().getAbsolutePath();

                try {
                    controller.setMaskingImage(maskingpath);
                } catch (IOException ex) {
                    showExceptionError(ex);
                    statusLabel.setText("Error while opening masking image!");
                }
            }
        });

        JMenuItem exit = new JMenuItem("Exit");
        exit.addActionListener((ActionEvent e) -> {
            int option = JOptionPane.showConfirmDialog(null, "Exit program?");
            if (option == JOptionPane.OK_OPTION) {
                System.exit(0);
            }
        });

        fileMenu.add(openImage);
        fileMenu.add(saveImage);
        fileMenu.add(openFilterImage);
        fileMenu.addSeparator();
        fileMenu.add(exit);

        menuBar.add(fileMenu);

        //********************************** VIEW MENU ****************************************

        JMenu viewMenu = new JMenu("View");
        JCheckBoxMenuItem showSettings = new JCheckBoxMenuItem("Show settings");
        showSettings.setState(true);
        showSettings.addItemListener((ItemEvent e) -> {
            settingsPanel.setVisible(showSettings.isSelected());
            frame.getContentPane().revalidate();
            frame.getContentPane().repaint();
            imagePanel.resetZoomFactor();
            double factor = imagePanel.getZoom();
            zoomButton.setText(new DecimalFormat("##.#").format(Double.valueOf(factor)) + "x");
            zoomSlider.setValue(Math.toIntExact(Math.round(factor * 100)));
        });
        viewMenu.add(showSettings);

        menuBar.add(viewMenu);

        return menuBar;
    }

    private JPanel buildDifferenceSettings() {

        JPanel diffPanel = new JPanel(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.anchor = GridBagConstraints.WEST;
        c.weightx = 1.0;
        c.weighty = 0.0;

        SpinnerModel modelLum = new SpinnerNumberModel(0, 0, 255, 1);

        lumField = new JSpinner(modelLum);
        JFormattedTextField spinnerTemp1 = ((JSpinner.NumberEditor) lumField.getEditor()).getTextField();
        ((NumberFormatter) spinnerTemp1.getFormatter()).setAllowsInvalid(false);
        //lumField.setMaximumSize(lumField.getPreferredSize());

        SpinnerModel modelDif = new SpinnerNumberModel(0, 0, 255, 1);

        difField = new JSpinner(modelDif);
        JFormattedTextField spinnerTemp2 = ((JSpinner.NumberEditor) difField.getEditor()).getTextField();
        ((NumberFormatter) spinnerTemp2.getFormatter()).setAllowsInvalid(false);
        //difField.setMaximumSize(difField.getPreferredSize());

        c.gridx = 0;
        c.gridy = 0;
        diffPanel.add(new JLabel("Luminance threshold:"), c);
        c.gridx = 0;
        c.gridy = 1;
        diffPanel.add(lumField, c);
        c.gridx = 0;
        c.gridy = 2;
        diffPanel.add(new JLabel("Difference threshold:"), c);
        c.gridx = 0;
        c.gridy = 3;
        diffPanel.add(difField, c);

        diffPanel.setVisible(true);

        return diffPanel;

    }

    private JPanel buildRandomSettings() { // TODO: same code as diffsettings

        JPanel randPanel = new JPanel(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.anchor = GridBagConstraints.WEST;
        c.weightx = 1.0;
        c.weighty = 0.0;

        SpinnerModel modelLum = new SpinnerNumberModel(1, 1, Integer.MAX_VALUE, 1);

        startField = new JSpinner(modelLum);
        JFormattedTextField spinnerTemp1 = ((JSpinner.NumberEditor) startField.getEditor()).getTextField();
        ((NumberFormatter) spinnerTemp1.getFormatter()).setAllowsInvalid(false); // Todo: more than 100 allowed
        //lumField.setMaximumSize(lumField.getPreferredSize());

        SpinnerModel modelDif = new SpinnerNumberModel(100, 1, Integer.MAX_VALUE, 1);

        endField = new JSpinner(modelDif);
        JFormattedTextField spinnerTemp2 = ((JSpinner.NumberEditor) endField.getEditor()).getTextField();
        ((NumberFormatter) spinnerTemp2.getFormatter()).setAllowsInvalid(false);
        //difField.setMaximumSize(difField.getPreferredSize());

        c.gridx = 0;
        c.gridy = 0;
        randPanel.add(new JLabel("Random range minimum:"), c);
        c.gridx = 0;
        c.gridy = 1;
        randPanel.add(startField, c);
        c.gridx = 0;
        c.gridy = 2;
        randPanel.add(new JLabel("Random range maximum:"), c);
        c.gridx = 0;
        c.gridy = 3;
        randPanel.add(endField, c);

        randPanel.setVisible(true);

        return randPanel;

    }

    /******************************************************************************************************************
     *                                                 Helper                                                         *
     ******************************************************************************************************************/

    private void showExceptionError(Exception ex) {
        StringWriter sw = new StringWriter();
        ex.printStackTrace(new PrintWriter(sw));
        String exceptionAsString = sw.toString();

        JOptionPane.showMessageDialog(null,
                "Oops, something went wrong... " + System.getProperty("line.separator") + exceptionAsString,
                "Oops", JOptionPane.ERROR_MESSAGE);

    }

    void finishedSorting() {
        imagePanel.stopAnimation();
        statusLabel.setText("Finished sorting!");
        sortButton.setText("Start sorting");
        finishedSorting = true;
    }

    void setController(PictureSorter controller) {
        this.controller = controller;
    }

    boolean isUsingMasking() {
        return useMasking;
    }

    boolean invertSort() {
        return invertSort;
    }

    boolean invertRandom() {
        return invertRandom;
    }

    boolean wrapLines() {
        return wrapLines;
    }

    PictureSorter.SortingMode getSortMode() {
        return sortMode;
    }

    PictureSorter.ComparisonMode getCompMode() {
        return compMode;
    }

    int getLuminanceThreshold() {
        return (int) lumField.getValue();
    }

    int getDifferenceThreshold() {
        return (int) difField.getValue();
    }

    int getRandomStart() {
        return (int) startField.getValue();
    }

    int getRandomEnd() {
        return (int) endField.getValue();
    }

    void setImage(BufferedImage image){
        imagePanel.setImage(image);
    }

    JSlider getZoomSlider() {
        return zoomSlider;
    }

    JButton getZoomButton() {
        return zoomButton;
    }

    int getRotation(){
        return rotSlider.getValue();
    }

}
