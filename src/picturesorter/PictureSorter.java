/*
 * Copyright (C) 2017 DasUngesagte
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package picturesorter;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Controls communication between Sorter and GUI
 *
 * @author DasUngesagte
 */
public class PictureSorter {

    public enum SortingMode {
        LINES,
        DIFFERENCE,
        RANDOM,
        RANDOM_GAPS
    }

    public enum ComparisonMode {
        LUMINANCE,
        LUMINANCE_WEIGHTED,
        RED,
        GREEN,
        BLUE,
        HSV,
        HSV_STEP
    }

    private static final PictureSorter INSTANCE = new PictureSorter();

    private final GUI gui;
    private Sorter sorter;

    private BufferedImage image;
    private BufferedImage sortedImage;
    private BufferedImage maskingImage;

    private int currentRotation;

    private PictureSorter() {
        gui = GUI.getInstance();
    }

    // Singleton
    private static PictureSorter getInstance() {
        return INSTANCE;
    }

    /******************************************************************************************************************
     *                                                 MAIN                                                           *
     ******************************************************************************************************************/

    public static void main(String[] args) {
        PictureSorter picsort = getInstance();
        picsort.gui.setController(picsort);
        picsort.gui.buildGUI();
    }

    /******************************************************************************************************************
     *                                               Starter                                                          *
     ******************************************************************************************************************/

    void initSort() {

        sorter = new Sorter();
        sorter.setOptions(gui.getSortMode(),
                gui.getCompMode(),
                gui.getLuminanceThreshold(),
                gui.getDifferenceThreshold(),
                gui.getRandomStart(),
                gui.getRandomEnd(),
                gui.isUsingMasking(),
                gui.invertSort(),
                gui.invertRandom(),
                gui.wrapLines());

        sorter.setImage(sortedImage);

        // Set controller for writing:
        sorter.setController(this);

        if (gui.isUsingMasking()){
            maskingImage = rotateImage(maskingImage, gui.getRotation());
            sorter.setMaskingImage(maskingImage);
        }

        Thread sortingThread = new Thread(sorter);
        sortingThread.start();
    }

    void finishedSorting() {
        gui.finishedSorting();
    }

    void cancelSort() {
        sorter.cancelSort();
    }

    void resetImage() {
        this.sortedImage = new BufferedImage(image.getWidth(), image.getHeight(), image.getType());
        Graphics2D g = this.sortedImage.createGraphics();
        g.drawImage(image, 0, 0, null);
        g.dispose();
    }

    synchronized void setSortedImage(BufferedImage sortedImage) {
        this.sortedImage = sortedImage;
        gui.setImage(sortedImage);
    }

    BufferedImage getImage() {
        return image;
    }

    /******************************************************************************************************************
     *                                                 GETTER                                                         *
     ******************************************************************************************************************/

    void setImage(String inputpath) throws IOException {
        this.image = ImageIO.read(new File(inputpath));

        // This gives a copy, not a reference (to retain the original)
        // Causes double memory usage, but enables fast resorting
        this.sortedImage = new BufferedImage(image.getWidth(), image.getHeight(), image.getType());
        Graphics2D g = this.sortedImage.createGraphics();
        g.drawImage(image, 0, 0, null);
        g.dispose();
    }

    void setMaskingImage(String inputpath) throws IOException {
        this.maskingImage = ImageIO.read(new File(inputpath));
    }

    void saveImage(String format, String outputpath) throws IOException {
        System.out.println("Saving " + format + " to " + outputpath);
        File outfile = new File(outputpath);
        ImageIO.write(sortedImage, format, outfile);
    }

    protected void rotateImage(int angle) {
        if(sortedImage == null) return;
        sortedImage = rotateImage(this.sortedImage, angle - currentRotation);  // This is a copy, not the image to be sorted
        gui.setImage(sortedImage);
        currentRotation = angle;
    }

    private BufferedImage rotateImage(BufferedImage image, double angle) {

        if(angle == 0.0) return image;

        double radians = Math.toRadians(angle);
        double sin = Math.abs(Math.sin(radians));
        double cos = Math.abs(Math.cos(radians));

        int w = image.getWidth();
        int h = image.getHeight();
        int newW = (int) Math.floor(w * cos + h * sin);
        int newH = (int) Math.floor(h * cos + w * sin);

        BufferedImage rotatedImage = new BufferedImage(newW, newH, image.getType());
        Graphics2D g2 = rotatedImage.createGraphics();
        AffineTransform rot = AffineTransform.getRotateInstance(radians, w / 2f, h / 2f);
        g2.translate((newW - w) / 2, (newH - h) / 2);
        g2.transform(rot);
        g2.drawImage(image, null, 0, 0);
        g2.dispose();

        return rotatedImage;

    }
}
