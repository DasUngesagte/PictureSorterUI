package picturesorter;


import javax.swing.filechooser.FileFilter;
import java.io.File;
import java.util.Arrays;
import java.util.List;

public class ImageFileFilter extends FileFilter {

    private String description;
    private String extensions[];

    ImageFileFilter(String description, String extension) {
        this(description, new String[]{extension});
    }

    ImageFileFilter(String description, String extensions[]) {
        if (description == null) {
            this.description = extensions[0];
        } else {
            this.description = description;
        }
        this.extensions = extensions.clone();

        List<String> extList = Arrays.asList(this.extensions);
        extList.replaceAll(String::toLowerCase);
        extList.toArray(this.extensions);
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public boolean accept(File file) {

        String path = file.getAbsolutePath().toLowerCase();

        for (String extension : extensions) {

            if ((path.endsWith(extension) && (path.charAt(path.length() - extension.length() - 1)) == '.')) {
                return true;
            }
        }

        return false;
    }
}
