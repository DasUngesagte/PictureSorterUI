/*
 * Copyright (C) 2017 DasUngesagte
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package picturesorter;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.util.Random;

/**
 * Handles image processing.
 *
 * @author DasUngesagte
 */
public class Sorter implements Runnable {

    private PictureSorter controller;

    private volatile boolean cancel;

    // Options:
    private PictureSorter.SortingMode sortingSwitcher;
    private PictureSorter.ComparisonMode comparisonSwitcher;
    private Random rand;
    private int luminanceThreshold;
    private int differenceThreshold;
    private int randomStart;
    private int randomEnd;
    private boolean useMasking;
    private boolean invertSort;
    private boolean invertRandom;
    private boolean wrapLines;

    private BufferedImage img;
    private BufferedImage imgMasking;
    private int[] data; // Colors by bit-value in linear array, use img dimensions to set new data
    private int[] dataMasking;

    private int getWeightedLuminance(int color) {

        int[] RGBValue = getRGBValue(color);

        return (int) Math.round(0.2126d * RGBValue[0] + 0.7152d * RGBValue[1] +
                0.0722d * RGBValue[2]);
    }

    private boolean luminanceThreshold(int color, int threshold) {

        int luminance = getWeightedLuminance(color);

        return luminance > threshold;
    }

    private int pixelDifference(int x, int y) {

        int luminanceX = getWeightedLuminance(x);
        int luminanceY = getWeightedLuminance(y);

        return Math.abs(luminanceX - luminanceY);

    }

    /******************************************************************************************************************
     *                                                 Sorting                                                        *
     ******************************************************************************************************************/

    private void mergeSort(int[] arrayToSort, int left, int right, SortingMethod sortingMethod) {

        int middle = (left + right) / 2;    // other strategies are available

        if (cancel) return;

        if (left < middle) mergeSort(arrayToSort, left, middle, sortingMethod);
        if (middle + 1 < right) mergeSort(arrayToSort, middle + 1, right, sortingMethod);

        if (cancel) return;

        merge(arrayToSort, left, middle, right, sortingMethod);
    }

    private void merge(int[] arrayToSort, int left, int middle, int right, SortingMethod sortingMethod) {

        int[] helper = new int[right - left + 1];
        int i = left;
        int j = middle + 1;

        for (int k = left; k < right + 1; k++) {

            int posX = img.getMinX() + (k % img.getWidth());
            int posY = img.getMinY() + Math.floorDiv(k, img.getWidth());


            if ((i > middle) || ((j <= right) && (sortingMethod.sortingMethod(arrayToSort[i], arrayToSort[j]) ^ invertSort))) {
                // arrayToSort[j] < arrayToSort[i]
                helper[k - left] = arrayToSort[j];
                j++;

                // Set new pixel, so we can show changes in the GUI:
                img.setRGB(posX, posY, helper[k - left]);

            } else {

                helper[k - left] = arrayToSort[i];
                i++;

                // Update this new pixel in the controller:
                img.setRGB(posX, posY, helper[k - left]);

            }
        }

        System.arraycopy(helper, 0, arrayToSort, left, right + 1 - left);
    }

    private void startSort(PictureSorter.ComparisonMode comparisonMode, int leftLimit,
                           int rightLimit, boolean useMasking) {

        if(invertRandom)
            this.invertSort = this.rand.nextBoolean();

        if (useMasking) {

            int[] newLimits = getMaskingLimits(leftLimit, rightLimit);
            int oldRightLimit;
            int oldLeftLimit;
            int newLeftLimit = newLimits[0];
            int newRightLimit = newLimits[1];

            do {
                switch (comparisonMode) {
                    case LUMINANCE_WEIGHTED:
                        mergeSort(data, newLeftLimit, newRightLimit, this::sortingWeightLuminanceWeighted);
                        break;
                    case LUMINANCE:
                        mergeSort(data, newLeftLimit, newRightLimit, this::sortingWeightLuminance);
                        break;
                    case RED:
                        mergeSort(data, newLeftLimit, newRightLimit, this::sortingWeightRed);
                        break;
                    case GREEN:
                        mergeSort(data, newLeftLimit, newRightLimit, this::sortingWeightGreen);
                        break;
                    case BLUE:
                        mergeSort(data, newLeftLimit, newRightLimit, this::sortingWeightBlue);
                        break;
                    case HSV:
                        mergeSort(data, newLeftLimit, newRightLimit, this::sortingWeightHSV);
                        break;
                    case HSV_STEP:
                        mergeSort(data, newLeftLimit, newRightLimit, this::sortingWeightHSVStep);
                        break;
                    default:
                        break;
                }

                if (newRightLimit == data.length - 1) break;

                oldLeftLimit = newLeftLimit;
                oldRightLimit = newRightLimit;
                newLimits = getMaskingLimits(newRightLimit + 1, rightLimit);
                newLeftLimit = newLimits[0];
                newRightLimit = newLimits[1];

            }
            while (newRightLimit != oldRightLimit && newLeftLimit != oldLeftLimit);

        } else {
            switch (comparisonMode) {
                case LUMINANCE_WEIGHTED:
                    mergeSort(data, leftLimit, rightLimit, this::sortingWeightLuminanceWeighted);
                    break;
                case LUMINANCE:
                    mergeSort(data, leftLimit, rightLimit, this::sortingWeightLuminance);
                    break;
                case RED:
                    mergeSort(data, leftLimit, rightLimit, this::sortingWeightRed);
                    break;
                case GREEN:
                    mergeSort(data, leftLimit, rightLimit, this::sortingWeightGreen);
                    break;
                case BLUE:
                    mergeSort(data, leftLimit, rightLimit, this::sortingWeightBlue);
                    break;
                case HSV:
                    mergeSort(data, leftLimit, rightLimit, this::sortingWeightHSVStep);
                    break;
                case HSV_STEP:
                    mergeSort(data, leftLimit, rightLimit, this::sortingWeightHSVStep);
                    break;
                default:
                    break;
            }
        }
    }

    /******************************************************************************************************************
     *                                                 Starter                                                        *
     ******************************************************************************************************************/

    private void initSort() {

        // Give the reference:
        controller.setSortedImage(img); // TODO: necessary?

        int height = img.getHeight();
        int width = img.getWidth();
        int size = height * width;

        switch (sortingSwitcher) {
            case LINES: {
                if(wrapLines){
                    startSort(comparisonSwitcher, 0, data.length - 1, useMasking);
                } else {
                    for (int k = 0; k < height; k++) {  // Invoke mergesort for each line
                        startSort(comparisonSwitcher, k * width, (k + 1) * width - 1, useMasking);
                    }
                }
                break;
            }

            case DIFFERENCE: {

                if(wrapLines){
                    int position = 0;

                    while (position < size) {

                        // Check for pixel over threshold:
                        while ((position < size - 1) && luminanceThreshold(data[position], luminanceThreshold)) {
                            position++;
                        }

                        mainloop:
                        while (!cancel) {
                            int secPosition = position + 1;

                            // Is this position already out of bounds?
                            if (secPosition >= size - 1) {
                                startSort(comparisonSwitcher, position, secPosition - 1, useMasking);
                                position = secPosition;
                                break;
                            }

                            while (pixelDifference(data[position], data[secPosition]) < differenceThreshold) {
                                secPosition++;
                                if (secPosition >= size - 1) {
                                    startSort(comparisonSwitcher, position, secPosition - 1, useMasking);
                                    position = secPosition;
                                    break mainloop;
                                }

                            }

                            if (secPosition != position + 1)
                                startSort(comparisonSwitcher, position, secPosition, useMasking);
                            position = secPosition;
                        }
                    }
                } else {
                    for (int i = 0; i < height; i++) {
                        int position = i * width;

                        // Check for pixel over threshold:
                        while ((position < (i + 1) * width - 1) && luminanceThreshold(data[position], luminanceThreshold)) {
                            position++;
                        }

                        // Else we've got a pixel over the threshold,
                        // so search for a pixel within the threshold:
                        mainloop:
                        // Label-break, yay
                        while (!cancel) {

                            int secPosition = position + 1;

                            // Is this position already out of bounds?
                            if (secPosition >= (i + 1) * width - 1) break;

                            while (pixelDifference(data[position], data[secPosition]) < differenceThreshold) {

                                secPosition++;
                                if (secPosition >= (i + 1) * width - 1) break mainloop;

                            }

                            if (secPosition != position + 1)
                                startSort(comparisonSwitcher, position, secPosition, useMasking);
                            position = secPosition;

                        }
                    }
                }

                break;
            }

            case RANDOM: {
                if(randomStart >= randomEnd) break;
                if(wrapLines){
                    int start = 0;
                    while (start < size) {
                        int end = start + rand.nextInt((randomEnd - randomStart) + 1) + randomStart;
                        end = end >= size ? size - 1 : end;
                        startSort(comparisonSwitcher, start, end, useMasking);
                        start = end + 1;
                    }
                } else {
                    for (int i = 0; i < height; i++) {
                        for (int j = 0; j < width;){
                            int end = j + rand.nextInt((randomEnd - randomStart) + 1) + randomStart;
                            end = end >= width ? width - 1 : end;
                            startSort(comparisonSwitcher, i * width + j, i * width + end, useMasking);
                            j = end + 1;
                        }
                    }
                }
                break;
            }

            case RANDOM_GAPS: {
                if(randomStart >= randomEnd) break;
                if(wrapLines){
                    int start = 0;
                    while (start < size) {
                        int end = start + rand.nextInt((randomEnd - randomStart) + 1) + randomStart;
                        end = end >= size ? size - 1 : end;
                        if(rand.nextBoolean()){
                            startSort(comparisonSwitcher, start, end, useMasking);
                        }
                        start = end + 1;
                    }
                } else {
                    for (int i = 0; i < height; i++) {
                        for (int j = 0; j < width;){
                            int end = j + rand.nextInt((randomEnd - randomStart) + 1) + randomStart;
                            end = end >= width ? width - 1 : end;
                            if(rand.nextBoolean()){
                                startSort(comparisonSwitcher, i * width + j, i * width + end, useMasking);
                            }
                            j = end + 1;
                        }
                    }
                }
                break;
            }

            default:
                break;
        }
    }

    @Override
    public void run() {

        this.initSort();
        controller.finishedSorting();

    }

    /******************************************************************************************************************
     *                                                 Helper                                                         *
     ******************************************************************************************************************/

    private int[] getRGBValue(int color) {

        int[] RGBValue = new int[3];

        RGBValue[2] = (color & 0xff);
        RGBValue[1] = (color & 0xff00) >> 8;
        RGBValue[0] = (color & 0xff0000) >> 16;

        /*  The RGB color is given per 32 bit integer and constructed like this:
         *   binary value:  1111 1111 0000 0000 0101 0101 1111 0000 (example)
         *   blue-bits:                         0000 0000 1111 1111
         *   green-bits:              0000 0000 1111 1111 0000 0000
         *   red-bits:      0000 0000 1111 1111 0000 0000 0000 0000
         *   alpha-bits:    1111 1111 0000 0000 0000 0000 0000 0000
         */

        return RGBValue;

    }

    private int[] getMaskingLimits(int left, int right) {

        int[] newLimits = new int[2];
        int newLeft = left;
        int newRight = right;

        for (int i = left; i <= right; i++) {

            newLeft = i;

            if (luminanceThreshold(dataMasking[i], 128)) break;
        }

        for (int i = newLeft + 1; i <= right; i++) {

            newRight = i;

            if (!luminanceThreshold(dataMasking[i], 128)) {
                newRight--;
                break;
            }
        }

        newLimits[0] = newLeft;
        newLimits[1] = newRight;

        return newLimits;

    }

    /******************************************************************************************************************
     *                                              Getter/Setter                                                     *
     ******************************************************************************************************************/

    void setController(PictureSorter controller) {

        this.controller = controller;

    }

    void setOptions(PictureSorter.SortingMode sortMode, PictureSorter.ComparisonMode compMode,
                    int luminanceThreshold, int differenceThreshold,
                    int randomStart, int randomEnd,
                    boolean useMasking, boolean invertSort, boolean invertRandom, boolean wrapLines) {

        this.rand = new Random();

        this.sortingSwitcher = sortMode;
        this.comparisonSwitcher = compMode;
        this.luminanceThreshold = luminanceThreshold;
        this.differenceThreshold = differenceThreshold;
        this.randomStart = randomStart;
        this.randomEnd = randomEnd;
        this.useMasking = useMasking;
        this.invertSort = invertSort;
        this.invertRandom = invertRandom;
        this.wrapLines = wrapLines;

    }

    void setImage(BufferedImage img) {

        this.img = img;
        this.data = img.getRGB(img.getMinX(), img.getMinY(), img.getWidth(), img.getHeight(), data, 0, img.getWidth());

    }

    void setMaskingImage(BufferedImage imgForMasking) {

        if (img == null) return;

        BufferedImage scaled = new BufferedImage(img.getWidth(), img.getHeight(), img.getType());
        Graphics2D g2 = scaled.createGraphics();
        g2.drawImage(imgForMasking, 0, 0, img.getWidth(), img.getHeight(), null); // It's this easy to scale....
        g2.dispose();

        this.imgMasking = scaled;
        this.dataMasking = scaled.getRGB(scaled.getMinX(), scaled.getMinY(), scaled.getWidth(),
                scaled.getHeight(), dataMasking, 0, scaled.getWidth());


    }

    void cancelSort() {
        cancel = true;
    }

    /******************************************************************************************************************
     *                                            Sorting methods                                                     *
     ******************************************************************************************************************/

    private boolean sortingWeightLuminance(int x, int y) {

        int[] valueX = getRGBValue(x);
        int[] valueY = getRGBValue(y);

        // Unweighted luminance:
        int luminanceX = valueX[0] + valueX[1] + valueX[2];
        int luminanceY = valueY[0] + valueY[1] + valueY[2];

        return luminanceX > luminanceY;

    }

    private boolean sortingWeightLuminanceWeighted(int x, int y) {

        int luminanceX = getWeightedLuminance(x);
        int luminanceY = getWeightedLuminance(y);

        return luminanceX > luminanceY;

    }

    private boolean sortingWeightRed(int x, int y){
        return sortingWeightColor(x,y,0,1,2);
    }

    private boolean sortingWeightGreen(int x, int y){
        return sortingWeightColor(x,y,1,0,2);
    }

    private boolean sortingWeightBlue(int x, int y){
        return sortingWeightColor(x,y,2,0,1);
    }

    private boolean sortingWeightColor(int x, int y, int primary, int secondary_one, int secondary_two){
        int[] valueX = getRGBValue(x);
        int[] valueY = getRGBValue(y);

        // Red: 0 1 2
        // Green: 1 0 2
        // Blue: 2 0 1
        double weightedX = 2. * valueX[primary] / (valueX[secondary_one] + valueX[secondary_two] + 1);
        double weightedY = 2. * valueY[primary] / (valueY[secondary_one] + valueY[secondary_two] + 1);

        return weightedX > weightedY;
    }

    private boolean sortingWeightHSVStep(int x, int y) {

        int[] valueX = getRGBValue(x);
        int[] valueY = getRGBValue(y);

        int lumX = (int) Math.round(Math.sqrt(0.2126d * valueX[0] + 0.7152d * valueX[1] +
                0.0722d * valueX[2]));
        int lumY = (int) Math.round(Math.sqrt(0.2126d * valueY[0] + 0.7152d * valueY[1] +
                0.0722d * valueY[2]));

        float[] hsvX = Color.RGBtoHSB(valueX[0], valueX[1], valueX[2], null);
        float[] hsvY = Color.RGBtoHSB(valueY[0], valueY[1], valueY[2], null);

        int hueX = Math.round((hsvX[0] / 255) * 8);
        int valX = Math.round((hsvX[2] / 255) * 8);

        if ((hueX / 255) % 2 == 1) {
            valX = 8 - valX;
            lumX = 8 - lumX;
        }

        int hueY = Math.round((hsvY[0] / 255) * 8);
        int valY = Math.round((hsvY[2] / 255) * 8);

        if ((hueY / 255) % 2 == 1) {
            valY = 8 - valY;
            lumY = 8 - lumY;
        }

        // tuple comparison:
        return hueX > hueY || lumX > lumY || valX > valY;

    }

    private boolean sortingWeightHSV(int x, int y) {

        int[] valueX = getRGBValue(x);
        int[] valueY = getRGBValue(y);

        float[] hsvX = Color.RGBtoHSB(valueX[0], valueX[1], valueX[2], null);
        float[] hsvY = Color.RGBtoHSB(valueY[0], valueY[1], valueY[2], null);

        // tuple comparison:
        return hsvX[0] > hsvY[0] || hsvX[1] > hsvY[1] || hsvX[2] > hsvY[2];

    }

    @FunctionalInterface
    interface SortingMethod {

        boolean sortingMethod(int x, int y);

    }

}
