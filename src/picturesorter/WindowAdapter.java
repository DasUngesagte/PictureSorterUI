/*
 * Copyright (C) 2017 DasUngesagte
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package picturesorter;

import javax.swing.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

/**
 * Adapter class.
 *
 * @author DasUngesagte
 */
public class WindowAdapter implements WindowListener {

    @Override
    public void windowClosing(WindowEvent event) {

        int option = JOptionPane.showConfirmDialog(null, "Exit?");
        if (option == JOptionPane.OK_OPTION) {
            System.exit(0);
        }

    }

    @Override
    public void windowClosed(WindowEvent event) { /* Empty */ }

    @Override
    public void windowDeiconified(WindowEvent event) { /* Empty */ }

    @Override
    public void windowIconified(WindowEvent event) { /* Empty */ }

    @Override
    public void windowActivated(WindowEvent event) { /* Empty */ }

    @Override
    public void windowDeactivated(WindowEvent event) { /* Empty */ }

    @Override
    public void windowOpened(WindowEvent event) { /* Empty */ }

}
