package picturesorter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.text.DecimalFormat;

public class MouseAdapter implements MouseListener, MouseMotionListener, MouseWheelListener {

    private GUI gui;
    private ImagePanel imagePanel;
    private JScrollPane imagePanelScrollPane;

    // Dragging
    private Point origin;

    MouseAdapter(GUI gui) {
        this.gui = gui;
    }

    void setImagePanel(ImagePanel imagePanel, JScrollPane imagePanelScrollPane) {

        this.imagePanel = imagePanel;
        this.imagePanelScrollPane = imagePanelScrollPane;
    }

    @Override
    public void mouseDragged(MouseEvent e) {

        if (origin != null) {
            JViewport viewPort = (JViewport) SwingUtilities.getAncestorOfClass(JViewport.class, imagePanel);
            if (viewPort != null) {
                int deltaX = origin.x - e.getX();
                int deltaY = origin.y - e.getY();

                Rectangle view = viewPort.getViewRect();
                view.x += deltaX;
                view.y += deltaY;

                imagePanel.scrollRectToVisible(view);
            }

        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {

        if (e.isControlDown()) {

            // Dispatch to scrollPane:
            imagePanel.getParent().dispatchEvent(e);

        } else {

            //Zoom in
            if (e.getWheelRotation() < 0) {

                double zoomFactor = this.imagePanel.getZoom() * 1.1;
                if (zoomFactor >= 0.1d && zoomFactor <= 5.0d) {

                    imagePanel.setZoomFactor(zoomFactor);
                    Point point = MouseInfo.getPointerInfo().getLocation();
                    SwingUtilities.convertPointFromScreen(point, imagePanelScrollPane);

                    Point pos = imagePanelScrollPane.getViewport().getViewPosition();

                    int newX = (int) (point.x * (1.1f - 1f) + 1.1f * pos.x);
                    int newY = (int) (point.y * (1.1f - 1f) + 1.1f * pos.y);
                    imagePanelScrollPane.getViewport().setViewPosition(new Point(newX, newY));


                    imagePanel.revalidate();
                    imagePanel.repaint();

                    gui.getZoomButton().setText(new DecimalFormat("##.#")
                            .format(Double.valueOf(imagePanel.getZoom())) + "x");
                    gui.getZoomSlider().setValue(Math.toIntExact(Math.round(imagePanel.getZoom() * 100)));
                }
            }

            //Zoom out
            if (e.getWheelRotation() > 0) {

                double zoomFactor = this.imagePanel.getZoom() * 0.9;
                if (zoomFactor >= 0.1d && zoomFactor <= 5.0d) {

                    imagePanel.setZoomFactor(zoomFactor);
                    Point point = MouseInfo.getPointerInfo().getLocation();
                    SwingUtilities.convertPointFromScreen(point, imagePanelScrollPane);

                    Point pos = imagePanelScrollPane.getViewport().getViewPosition();

                    int newX = (int) (point.x * (0.9f - 1f) + 0.9f * pos.x);
                    int newY = (int) (point.y * (0.9f - 1f) + 0.9f * pos.y);
                    imagePanelScrollPane.getViewport().setViewPosition(new Point(newX, newY));


                    imagePanel.revalidate();
                    imagePanel.repaint();

                    gui.getZoomButton().setText(new DecimalFormat("##.#")
                            .format(Double.valueOf(imagePanel.getZoom())) + "x");
                    gui.getZoomSlider().setValue(Math.toIntExact(Math.round(imagePanel.getZoom() * 100)));
                }
            }
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {

        if (e.getButton() == MouseEvent.BUTTON2) {
            // Reset zoom:
            imagePanel.resetZoomFactor();
            double factor = imagePanel.getZoom();
            gui.getZoomButton().setText(new DecimalFormat("##.#").format(Double.valueOf(factor)) + "x");
            gui.getZoomSlider().setValue(Math.toIntExact(Math.round(factor * 100)));
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        origin = new Point(e.getPoint());
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }
}
